import re

def replace_dirty_chars(dirty_str):

    # according to cBioPortal documentation: sample can contain only numbers, letters, points, underscores and hyphens
    nice_str = re.sub('[^A-Za-z0-9._-]+', '_', dirty_str)
    return nice_str


def prepare_for_cbp(ids):

    if isinstance(ids, str):
        return replace_dirty_chars(ids)

    retval = set()
    for val in ids:
        retval.add(replace_dirty_chars(val))

    return retval


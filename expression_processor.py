import utils
import conf
from apihandler import ApiHandler

APPLICATION = conf.APPLICATION
TMP = conf.TMP
EXP = conf.EXP


class Expression:
    """
       This class process expression data type and write them to file structure defined by cBio
    """

    def __init__(self, metadata, study, tmp_id_study, ids):
        self.metadata = metadata
        self.study = study
        self.tmp_id_study = tmp_id_study
        self.ids = ids

    def process(self):
        result = ApiHandler.call(APPLICATION + TMP + "/" + self.tmp_id_study + EXP + '/' + self.metadata['stable_id'],
                                  'expressions for tmplist' + self.tmp_id_study)
        ids = set()
        expressions = self.process_expressions(result, ids)
        if len(expressions) == 0:
            return False
        with open(conf.OUTPUT_DIR + self.study['name'] + '/' + self.metadata['data_filename'], 'w') as output:
            self.write(expressions, ids, output)
        self.write_metadata()
        return True

    def write_metadata(self):
        with open(conf.OUTPUT_DIR + self.study['name'] + "/meta_" + self.metadata['data_filename'], 'w') as output:
            output.write('cancer_study_identifier: ' + self.study['cancer_study_identifier'] + '\n')
            output.write('genetic_alteration_type: ' + self.metadata['genetic_alteration_type'] + '\n')
            output.write('datatype: ' + self.metadata['datatype'] + '\n')
            output.write('stable_id: ' + self.metadata['stable_id'] + '\n')
            output.write('show_profile_in_analysis_tab: ' + self.metadata['show_profile_in_analysis_tab'] + '\n')
            output.write('profile_name: ' + self.metadata['profile_name'] + '\n')
            output.write('profile_description: ' + self.metadata['profile_description'] + '\n')
            output.write('data_filename: ' + self.metadata['data_filename'] + '\n')

    def insert(self, array, ids, exp, id):
        value = 'value_of'
        array[ids.get(id)] = str(float(exp[value]))
        return array

    def process_expressions(self, expression_with_pdxmodel_info, ids_array):
        dic = {}
        ids = {}
        i = 0
        entrez = 'entrez_gene_id'
        hugo = 'hugo_symbol'
        for id in expression_with_pdxmodel_info:
            for exp in id["expressions"]:
                ids[exp['sample_id']] = i
                ids_array.add(exp['sample_id'])
                self.ids.append(exp['sample_id'])
            i += 1
        for r in expression_with_pdxmodel_info:
            for exp in r['expressions']:
                for e in exp["expressions"]:
                    entrez_id = str(e[entrez])
                    hugo_symbol = str(e[hugo])
                    dic[(hugo_symbol, entrez_id)] = self.insert(dic.get((hugo_symbol, entrez_id), ['NA'] * len(ids_array)), ids, e, exp['sample_id'])
        return dic

    def write(self, exp, ids, output):
        delim = '\t'
        output.write('Hugo_Symbol' + delim + 'Entrez_Gene_Id' + delim + delim.join(utils.prepare_for_cbp(ids)) + '\n')
        for key in exp:
            output.write(key[0] + delim + key[1] + delim + delim.join(
                exp[key]) + '\n')
import os
import socket

VERSION = '/v1'
# DATAHUB = os.environ.get("DATAHUB", "datahub-beta")
# DATAHUB = os.environ.get("DATAHUB", "datahub")
HOSTNAME = socket.gethostname()
DATAHUB = os.environ.get("DATAHUB", HOSTNAME)

# http - e.g. datahub.edirex.ics.muni.cz
#APPLICATION = 'https://' + DATAHUB + '.edirex.ics.muni.cz:5000/api' + VERSION
APPLICATION = os.environ.get("DATAHUB_APPLICATION",'https://' + DATAHUB + '.edirex.ics.muni.cz:5000/api' + VERSION)

# https - e.g. sanbox1-dev.edirex.ics.muni.cz
#APPLICATION = 'https://' + DATAHUB + '.edirex.ics.muni.cz:5000/api' + VERSION

# ToDo: unify http vs. https somehow

STUDY = '/studies'
SAMPLES = '/samples'
TMP = '/tmplists'
PDXMODELS = '/pdxmodels'
METAFILES = '/metafiles'
CANCERTYPES = '/cancertypes'
CNAS = '/cnas'
MUT = '/mutations'
EXP = '/expressions'
LOGNAME = 'log.txt'
RETRIES = 3

CBIO = "cbio"
SSWORD = "P@ssword1"
DB_Config = "cbioportal"
PORT = 3306
CON_TIMEOUT = 10

OUTPUT_DIR = '_studies/'

# ID = os.environ.get('ID', 196) # Curie-LC
ID = os.environ.get('ID', 2) # NKIX study
# ID = os.environ.get('ID', 234) # VHIO-CRC


# HOST = os.environ.get('DBHOST', 'test')
# DB_INTERFACE = None

# END



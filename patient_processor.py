import logging
import conf

class Patient:

    def __init__(self, metadata, study, models):
        self.metadata = metadata
        self.study = study
        self.models = models

    def write_metadata(self):
        with open(conf.OUTPUT_DIR + self.study['name'] + '/' + 'meta_' + self.metadata['data_filename'], 'w') as output:
            output.write('cancer_study_identifier: ' + self.study['cancer_study_identifier'] + '\n')
            output.write('genetic_alteration_type: ' + self.metadata['genetic_alteration_type'] + '\n')
            output.write('datatype: ' + self.metadata['datatype'] + '\n')
            output.write('data_filename: ' + self.metadata['data_filename'] + '\n')

    def write_header_patients(self, output):
        output.write("#Patient_Identifier\tINSTITUTION\n")
        output.write("#Identifier to uniquely specify a patient.\tINSTITUTION\n")
        output.write("#STRING\tSTRING\n")
        output.write("#1\t1\n")
        output.write("PATIENT_ID\tINSTITUTION\n")

    def write_patients(self, output):
        patient_ids = []
        for model in self.models:
            if model['patient_id'] in patient_ids:
                continue
            else:
                output.write(model['patient_id'] + '\t' +
                             model['institution'] + '\n')
                patient_ids.append(model['patient_id'])

    def process(self):
        logging.debug('Writing Patients data')
        with open(conf.OUTPUT_DIR + self.study['name'] + '/' + self.metadata['data_filename'], 'w') as output:
            self.write_header_patients(output)

            self.write_patients(output)

        logging.debug('Writing patients metadata')
        self.write_metadata()
        logging.debug("Patients done")

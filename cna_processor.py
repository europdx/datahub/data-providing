from apihandler import ApiHandler
import conf
import utils

APPLICATION = conf.APPLICATION
TMP = conf.TMP
CNAS = conf.CNAS


class Cna:
    """
    This class process cna data type and write them to file structure defined by cBio
    """
    def __init__(self, metadata, study, tmp_id_study, ids):
        self.metadata = metadata
        self.study = study
        self.tmp_id_study = tmp_id_study
        self.ids = ids

    def process(self):
        result = ApiHandler.call(APPLICATION + TMP + '/' + self.tmp_id_study + CNAS + '/' + self.metadata['stable_id'],
                                  'cnas for tmplist: ' + self.tmp_id_study)
        if len(result) == 0:
            return False
        ids = set()
        cnas = self.process_cna(result, ids, self.metadata['stable_id'])
        with open(conf.OUTPUT_DIR + self.study['name'] + '/' + self.metadata['data_filename'], 'w') as output:
            self.write(cnas, ids, output)
        self.write_metadata()
        return True

    def write_metadata(self):
        with open(conf.OUTPUT_DIR + self.study['name'] + '/meta_' + self.metadata['data_filename'], 'w') as output:
            output.write('cancer_study_identifier: ' + self.study['cancer_study_identifier'] + '\n')
            output.write('genetic_alteration_type: ' + self.metadata['genetic_alteration_type'] + '\n')
            output.write('datatype: ' + self.metadata['datatype'] + '\n')
            output.write('stable_id: ' + self.metadata['stable_id'] + '\n')
            output.write('show_profile_in_analysis_tab: ' + self.metadata['show_profile_in_analysis_tab'] + '\n')
            output.write('profile_name: ' + self.metadata['profile_name'] + '\n')
            output.write('profile_description: ' + self.metadata['profile_description'] + '\n')
            output.write('data_filename: ' + self.metadata['data_filename'] + '\n')

    def insert(self, array, ids, cna, id, stable_id):
        """
        Insert cna value on right place in array
        :param array: of values for entrez_gene_id_
        :param ids: dictionary key: pdxmodel id, value: position in array
        :param cna: cna object
        :param id: pdxmodel id
        :return: array with inserted value
        """
        value = 'value_of'
        if stable_id == 'log2CNA':
            array[ids.get(id)] = str(float(cna[value]))
        else:
            array[ids.get(id)] = str(int(cna[value]))
        return array

    def process_cna(self, cna_with_pdxmodel_info, ids_array, stable_id):
        """
        Make dictionary containing key: entrez gene, value: array containing values of cna ordered by pdxmodel
        Missing values of cna for pdxmodel is replaced with NA
        :param cna_with_pdxmodel_info: resource from api
        :param ids_array: array containing pdxmodel's ids order
        :return: dictionary containing key: entrez gene, value: array containing values of cna ordered by pdxmodel
        """
        dic = {}
        entrez = 'entrez_gene_id'
        hugo = "hugo_symbol"
        ids = {}
        i = 0
        for id in cna_with_pdxmodel_info:
            for c in id["cnas"]:
                ids[c['sample_id']] = i
                ids_array.add(c['sample_id'])
                self.ids.append(c['sample_id'])

            # ids_array.append(id['pdxmodel_id'])
            # self.ids.append(id['pdxmodel_id'])
            i += 1
        for r in cna_with_pdxmodel_info:
            for cna in r['cnas']:
                for c in cna["cnas"]:
                    entrez_id = str(c[hugo])
                    dic[entrez_id] = self.insert(dic.get(entrez_id, ['NA'] * len(ids_array)), ids, c, cna['sample_id'], stable_id)
        return dic

    def write(self, cnas, ids, output):
        """
        :param cnas: key: entrez_gene_id, value: -2, 2, 0, -1, 1, NA
        :param ids: pdxmodels ids
        :param output: file to write
        :return: void
        """
        delim = '\t'
        # output.write('Entrez_Gene_Id' + delim + delim.join(ids) + '\n')
        output.write('Hugo_Symbol' + delim + delim.join(utils.prepare_for_cbp(ids)) + '\n')
        for key in cnas:
            output.write(key + delim + delim.join(
                cnas[key]) + '\n')

#!/usr/bin/python3

import conf
import os
import sys
import json as js
import logging
import requests as api
import utils
#from MySQLdb import OperationalError
from db_interface import Database, Fake
from apihandler import ApiHandler
from cancer_type_processor import CancerType
from case_list_processor import CaseList
from cna_processor import Cna
from expression_processor import Expression
from mutation_processor import Mutation
from patient_processor import Patient
from sample_processor import Sample

VERSION = conf.VERSION
DATAHUB = conf.DATAHUB
APPLICATION = conf.APPLICATION
STUDY = conf.STUDY
SAMPLES = conf.SAMPLES
TMP = conf.TMP
PDXMODELS = conf.PDXMODELS
METAFILES = conf.METAFILES
CANCERTYPES = conf.CANCERTYPES
CNAS = conf.CNAS
MUT = conf.MUT
EXP = conf.EXP
LOGNAME = conf.LOGNAME
RETRIES = conf.RETRIES

CBIO = conf.CBIO
SSWORD = conf.SSWORD
DB_Config = conf.DB_Config
PORT = conf.PORT
CON_TIMEOUT = conf.CON_TIMEOUT

ID = conf.ID

#HOST = os.environ.get('DBHOST', 'test')

DB_INTERFACE = None



logging.basicConfig(format='%(asctime)s - %(message)s',
                    level=logging.DEBUG,
                    filename=LOGNAME,
                    filemode='a'
                    )
logging.getLogger().setLevel(logging.DEBUG)


# TODO cancer_type contain id not value -> https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker/issues/78
def write_study(name, study):
    with open(conf.OUTPUT_DIR + name, 'w') as output:
        output.write('type_of_cancer: ' + study['oncotree_code'] + '\n' +
                     'cancer_study_identifier: ' + study['cancer_study_identifier'] + '\n' +
                     'name: ' + study['name'] + '\n' +
                     'description: ' + study['description'] + '\n' +
                     'short_name: ' + study['short_name'] + '\n' +
                     'add_global_case_list: true' + '\n')


def post_ids(ids):
    """
    Post new tmplist with ids for each study
    :param ids: Ids to post
    :return: tmplist_id for ids
    """
    logging.debug('Posting ids')
    ids_array = []
    for sample in ids:
        ids_array.append(sample['model_id'])
    response = api.post(APPLICATION + TMP, json={'pdxmodel_list': ids_array})
    tmp_id = js.loads(response.content.decode('utf-8'))
    logging.debug('tmp id ' + str(tmp_id['tmplistid']))
    return tmp_id['tmplistid']


# TODO spaces in attributes  -> https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker/issues/75
# TODO https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker/issues/75  -> missing expression data type
def process_by_meta_files(meta_files, study, tmp_id_study, samples):
    """
    We process data base on their type
    :param meta_files: metafiles for study
    :param study: study object
    :param tmp_id_study: tmp_id which we created to be used for API request
    :param samples:
    :return: void
    """
    cnas = set()
    mut = set()
    for metadata in meta_files:
        metadata_type = metadata['datatype']
        stable_id = metadata['stable_id']
        if stable_id != "":
            if stable_id == "mutations":
                ids = []
                if Mutation(metadata, study, tmp_id_study, ids).process():
                    CaseList(study,  metadata['stable_id'], metadata['profile_name'], metadata['profile_description'],
                             set(ids)).process()
                    mut.update(ids)
            if stable_id == "gistic" or stable_id == 'log2CNA':
                ids = []
                if Cna(metadata, study, tmp_id_study, ids).process():
                    CaseList(study, metadata['stable_id'], metadata['profile_name'], metadata['profile_description'],
                             set(ids)).process()
                    cnas.update(ids)
            if stable_id in ["rna_seq_mrna", "rna_seq_mrna_capture", "rna_seq_mrna_capture_Zscores"]:
                ids = []
                if Expression(metadata, study, tmp_id_study, ids).process():
                    CaseList(study,  metadata['stable_id'], metadata['profile_name'], metadata['profile_description'],
                             set(ids)).process()
        else:
            if metadata_type == 'SAMPLE_ATTRIBUTES':
                Sample(metadata, study, samples).process()
            if metadata_type == "PATIENT_ATTRIBUTES":
                Patient(metadata, study, samples).process()
            if metadata_type == 'CANCER_TYPE':
                CancerType(metadata, study).process()

    if len(mut) != 0:
        CaseList(study, 'sequenced', 'Mutations all', 'All samples with mutations', mut).process()
    if len(cnas) != 0:
        CaseList(study, 'cna', 'Cnas all', 'All samples with cnas', cnas).process()


def create_data(study, ids):
    """
    We post ids to /tmplists and then we use them for communication with API.
    Grab metafiles for study and continue with them
    :param study: study object
    :param ids: ids of models to be imported
    :return: void
    """
    tmp_id_study = str(post_ids(ids))
    meta_files = ApiHandler.call(APPLICATION + METAFILES + '/' + study['name'], 'meta files')
    process_by_meta_files(meta_files, study, tmp_id_study, ids)
    logging.debug('created data for ' + study['name'])


def parallel_processing(samples_for_studies, studies):
    """
    For each study we can run subprocess
    """
    for key in samples_for_studies:
        create_data(studies[key], samples_for_studies[key])


def imported():
    """
    To find out if import succeeded is to query database or to has flag represented in persistent storage
    this is important because we can not determine if container was not restarted during import and also we start
    this script by hook which is activated each time container start up (eg. after moved to another node )
    :return: true if imported otherwise false
    """
    global DB_INTERFACE
    if HOST == 'test':
        DB_INTERFACE = Fake()
        return False
    db = get_connection()

    i = 0
    while not db:
        db = get_connection()
        i += 1
        if i == RETRIES:
            raise RuntimeError('DB is unreachable')
    logging.info("Connection required")
    DB_INTERFACE = Database(db)
    DB_INTERFACE.start_initialization()
    return DB_INTERFACE.is_imported()


def get_connection():
    import MySQLdb
    logging.info('Trying to connect to DB')
    try:

        db = MySQLdb.connect(host=HOST,
                             user=CBIO,
                             passwd=SSWORD,
                             db=DB_Config,
                             port=PORT,
                             connect_timeout=CON_TIMEOUT
                             )
    except OperationalError:
        logging.error('Can not connect to DB')
        return None
    logging.info("DB connection retrieved")
    return db


def write_to_file(stdout_of_process, study):
    with open(conf.OUTPUT_DIR + study + '_log.txt', 'w') as log:
        log.write(stdout_of_process)


def import_data(studies):
    """
    Import studies
    :return: void
    """

    for study in list(studies.values()):
        command = 'cbioportalImporter.py -s ./' + study['name']
        import subprocess
        logging.debug("importing data for " + study['name'])
        stdout_of_process = subprocess.getoutput(command)
        write_to_file(stdout_of_process, study['name'])
        if 'Updating study status to :' in stdout_of_process:
            logging.debug("imported data for " + study['name'])
        else:
            logging.debug("failed to import data for " + study['name'])
            return -1
    DB_INTERFACE.flag_imported()
    return 0


def create_study(study):
    """
    Creates study folder and write a study metadata
    :param study:
    :return:
    """
    try:
        os.mkdir(conf.OUTPUT_DIR)
    except FileExistsError:
        logging.warning('Folder for studies was already created')

    try:
        os.mkdir(conf.OUTPUT_DIR + study['name'])
    except FileExistsError:
        logging.warning('Folder for study ' + study['name'] + ' was already created')

    try:
        os.mkdir(conf.OUTPUT_DIR + study['name'] + '/case_lists')
    except FileExistsError:
        logging.warning('Folder for study ' + study['name'] + '/case_lists was already created')
    write_study(study['name'] + '/meta_study.txt', study)


def main():
    """
    Get all samples for each studies and then remove samples which user did not select.
    Then we can apply parallel processing for each study.
    """

    root = logging.getLogger()

    handler = logging.StreamHandler(sys.stdout)
    root.addHandler(handler)

    logging.debug('----- Hello world / Ahoj svete ----- ')

    #if imported():
    #    exit(0)
    #DB_INTERFACE.start_initialization()
    studies = ApiHandler.call(APPLICATION + STUDY, 'studies')
    samples_for_studies = {}
    study_dic = {}
    tmp_id = str(ID)

    logging.debug('tmplist ID: ' + tmp_id)


    all_samples = ApiHandler.call(APPLICATION + TMP + '/' + tmp_id + PDXMODELS, 'pdxmodels for tmplist: ' + tmp_id)
    for study in studies:
        all_samples_for_study = ApiHandler.call(APPLICATION + STUDY + '/' + study['name'] + PDXMODELS,
                                                    'Getting models for ' + study['name'])['pdxmodels']
        samples_for_study = [i for i in all_samples_for_study if i in all_samples]

        meta_files = ApiHandler.call(APPLICATION + METAFILES + '/' + study['name'], 'meta files')

        if len(samples_for_study) == 0 or len(meta_files) == 0:
            continue
        samples_for_studies[study['name']] = samples_for_study
        study_dic[study['name']] = study
        create_study(study)
    if not samples_for_studies:
        exit(0)
    parallel_processing(samples_for_studies, study_dic)
    #exit(import_data(study_dic))
    exit(0)


main()
